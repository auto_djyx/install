# -*- coding: utf-8 -*-
import os

url_prefix = 'http://fishros.com/install/install1s/'

base_url = url_prefix+'tools/base.py'

tools = {
    1: {'tip': '一键配置:禁用显卡图形模式和nouveau(温馨提醒,新电脑请在进入电脑Bios修改此项—>Boot—>Boot Display Configuration—>Expansion Card Text—>Disabled)',      'type': 0,     'tool': url_prefix+'tools/tool_install_1.py', 'dep': []},
    2: {'tip': '一键安装:ROS(支持ROS和ROS2,树莓派Jetson)',                 'type': 0,     'tool': url_prefix+'tools/tool_install_ros.py', 'dep': [18, 19]},
    3: {'tip': '一键安装:Nvidia显卡驱动',      'type': 0,     'tool': url_prefix+'tools/tool_install_2.py', 'dep': []},
    4: {'tip': '半手动安装:CUDA',      'type': 0,     'tool': url_prefix+'tools/tool_install_3.py', 'dep': []},
    5: {'tip': '一键安装:CMAKE ',      'type': 0,     'tool': url_prefix+'tools/tool_install_4.py', 'dep': []},
    6: {'tip': '一键安装:eigen-3.3.7 ',      'type': 0,     'tool': url_prefix+'tools/tool_install_5.py', 'dep': []},
    7: {'tip': '一键安装:opencv 3.4.5 ',      'type': 0,     'tool': url_prefix+'tools/tool_install_6.py', 'dep': []},
    8: {'tip': '一键设置:USB权限 ',      'type': 0,     'tool': url_prefix+'tools/tool_install_7.py', 'dep': []},
    9: {'tip': '一键配置:autoware依赖 ',      'type': 0,     'tool': url_prefix+'tools/tool_install_8.py', 'dep': []},
    10: {'tip': 'autoware依赖补丁 ',      'type': 0,     'tool': url_prefix+'tools/tool_install_9.py', 'dep': []},
    18: {'tip': '一键配置:ROS环境(快速更新ROS环境设置,自动生成环境选择)',     'type': 2,     'tool': url_prefix+'tools/tool_config_rosenv.py', 'dep': []},
    19: {'tip': '一键配置:系统源(更换系统源,支持全版本Ubuntu系统)',           'type': 2,    'tool': url_prefix+'tools/tool_config_system_source.py', 'dep': [1]},
    20: {'tip': '一键安装:VsCode开发工具',      'type': 0,     'tool': url_prefix+'tools/tool_install_vscode.py', 'dep': []},
    21: {'tip': '一键安装:向日葵',      'type': 0,     'tool': url_prefix+'tools/tool_install_SunloginClient.py', 'dep': []},
    22: {'tip': '一键安装:微软Edge',      'type': 0,     'tool': url_prefix+'tools/tool_install_microsoft_edge.py', 'dep': []},
    23: {'tip': '一键安装:google浏览器',      'type': 0,     'tool': url_prefix+'tools/tool_install_google.py', 'dep': []},
    24: {'tip': '一键安装:Todesk',      'type': 0,     'tool': url_prefix+'tools/tool_install_todesk.py', 'dep': []},
    25: {'tip': '一键安装:CloudCompare(安装点云软件)',      'type': 0,     'tool': url_prefix+'tools/tool_install_CloudCompare.py', 'dep': []},
    33: {'tip': '一键安装三剑筒:V向微',      'type': 0,     'tool': url_prefix+'tools/tool_install_VsCode_SunloginClient_edge.py', 'dep': []},
    99: {'tip': '一键配置:测试测试测试测试测试测试 ',      'type': 0,     'tool': url_prefix+'tools/1.py', 'dep': []},
        # 2: {'tip': '一键安装:github桌面版(小鱼常用的github客户端)',             'type': 0,     'tool': url_prefix+'tools/tool_install_github_desktop.py', 'dep': []},
    # 3: {'tip': '一键配置:rosdep(小鱼的rosdepc,又快又好用)',                 'type': 2,    'tool': url_prefix+'tools/tool_config_rosdep.py', 'dep': []},
    # 6: {'tip': '一键安装:nodejs',      'type': 0,     'tool': url_prefix+'tools/tool_install_nodejs.py', 'dep': []},
    # 8: {'tip': '一键安装:Docker',      'type': 0,     'tool': url_prefix+'tools/tool_install_docker.py', 'dep': []},
    # 9: {'tip': '一键安装:Cartographer(内测版v0.1)',      'type': 0,     'tool': url_prefix+'tools/tool_install_cartographer.py', 'dep': [3]},
    # 10: {'tip': '一键安装:微信(可以在Linux上使用的微信)',      'type': 0,     'tool': url_prefix+'tools/tool_install_wechat.py', 'dep': [8]},
    # 11: {'tip': '一键安装:ROS+Docker(支持所有版本ROS/ROS2)',                'type': 0,    'tool': url_prefix+'tools/tool_install_ros_with_docker.py', 'dep': [7, 8]},
    # 12: {'tip': '一键安装:PlateformIO MicroROS开发环境(支持Fishbot)',      'type': 0,     'tool': url_prefix+'tools/tool_install_micros_fishbot_env.py', 'dep': []},
    # 12: {'tip': '一键安装:PlateformIO MicroROS开发环境(支持Fishbot)',      'type': 0,     'tool': url_prefix+'tools/tool_install_micros_fishbot_env.py', 'dep': []},
    # 77: {'tip': '测试模式:运行自定义工具测试'},
}
#


def main():
    # download base
    # os.system("wget {} -O /tmp/fishinstall/{} --no-check-certificate".format(
    #     base_url, base_url.replace(url_prefix, '')))
    from tools.base import CmdTask, FileUtils, PrintUtils, ChooseTask
    from tools.base import encoding_utf8, osversion, osarch
    from tools.base import run_tool_file, download_tools
    from tools.base import config_helper
    # PrintUtils.print_delay(f"检测到你的系统版本信息为{osversion.get_codename()},{osarch}",0.001)

    # check base config
    if not encoding_utf8:
        print("Your system encoding not support ,will install some packgaes..")
        CmdTask("sudo apt-get install language-pack-zh-hans -y", 0).run()
        CmdTask("sudo apt-get install apt-transport-https -y", 0).run()
        FileUtils.append("/etc/profile", 'export LANG="zh_CN.UTF-8"')
        print('Finish! Please Try Again!')
        print('Solutions: https://fishros.org.cn/forum/topic/24 ')
        return False
    PrintUtils.print_success("基础检查通过...")

    book = """
                        .-~~~~~~~~~-._       _.-~~~~~~~~~-.
                    __.'              ~.   .~              `.__
                .'//     以道为径       \./       以简为本     \\\ `.
              .'//     以优为行          |      亦为道简优行     \\\ `.
            .'// .-~"""""""~~~~~~~-.______     |     ______.-~~~~~~~"""""""~-. \\\`.
          .'//.-"                   `-.  |  .-'                   "-.\\\`.
        .'//______.==============-..   \ | /   ..-==============.______\\\`.
        .'______________________________\|/______________________________`
        ----------------------------------------------------------------------"""

    tip = """===============================================================================
============欢迎使用一键安装工具，人生苦短，三省吾身，省时省力省心!============
===================请放心食用，无害无毒无异味，简称三无产品====================
===============================================================================
    """
    end_tip = """===============================================================================
如果觉得工具好用,请给个star,如果你想和小鱼一起编写工具,请关注微信公众号<鱼香ROS>,联系小鱼
更多工具教程，请访问鱼香ROS官方网站:http://fishros.com
    """
    i =True
    while i:
        PrintUtils.print_delay(tip, 0.001)
        PrintUtils.print_delay(book, 0.001)

        # 下载工具
        choose_dic = {}
        for tool_id in tools.keys():
            choose_dic[tool_id] = tools[tool_id]["tip"]

        code = -1
        while code != 0:
            code, result = ChooseTask(choose_dic, "---众多工具，等君来用---").run()
            
            if code == 0:
                PrintUtils().print_success("是觉得没有合胃口的菜吗？那就不要吃了！吃粑粑去！")
                i=False
             
            elif code == 77:
                code, result = ChooseTask(choose_dic, "请选择你要测试的程序:").run()
                if code < 0 and code >= 77:
                    return False
                run_tool_file(tools[code]['tool'].replace(url_prefix, '').replace("/", "."))
                
            else:
                download_tools(code, tools)
                run_tool_file(tools[code]['tool'].replace(url_prefix, '').replace("/", "."))

            user_choice = input("安装完毕，继续安装其他工具请输入1，退出请输入0：")
            if user_choice ==  "0":
                i = False  # 用户选择退出
                break


        config_helper.gen_config_file()
        PrintUtils.print_delay("客观吃完啦~~~", 0.1)
        PrintUtils.print_success("客观欢迎下次光临哦~~~", 0.001)
        PrintUtils.print_success("客观一路走好哦~~~", 0.001)

if __name__ == '__main__':
    main()
