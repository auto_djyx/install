# -*- coding: utf-8 -*-
from .base import BaseTool
from .base import PrintUtils,CmdTask,FileUtils,AptUtils,ChooseTask
from .base import osversion,osarch
from .base import run_tool_file

class Tool(BaseTool):
    def __init__(self):
        self.name = "一键安装todesk"
        self.type = BaseTool.TYPE_INSTALL
        self.autor = 'xxrz'

    def install_todesk(self):
        PrintUtils.print_info("开始安装todesk~")
        CmdTask("sudo dpkg -i ~/autoware/Software/todesk-v4.3.1.0-amd64.deb").run()
        PrintUtils.print_info("安装完成~")

    def run(self):
        self.install_todesk()

