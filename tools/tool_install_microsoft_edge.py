# -*- coding: utf-8 -*-
from .base import BaseTool
from .base import PrintUtils,CmdTask,FileUtils,AptUtils,ChooseTask
from .base import osversion,osarch
from .base import run_tool_file

class Tool(BaseTool):
    def __init__(self):
        self.name = "一键安装Edge"
        self.type = BaseTool.TYPE_INSTALL
        self.autor = 'xxrz'

    def install_microsoft_edge(self):
        PrintUtils.print_info("开始安装Edge~")
        CmdTask("sudo dpkg -i ~/autoware/Software/microsoft-edge-stable_114.0.1823.67-1_amd64.deb").run()
        PrintUtils.print_info("安装完成~")

    def run(self):
        self.install_microsoft_edge()

