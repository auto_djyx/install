# -*- coding: utf-8 -*-
from .base import BaseTool
from .base import PrintUtils, CmdTask, FileUtils, AptUtils, ChooseTask
from .base import osversion, osarch
from .base import run_tool_file

#  PrintUtils-打印信息
#  CmdTask-运行命令
#  FileUtils-文件操作
#  AptUtils-软件包管理
#  ChooseTask-显示选项列表


class Tool(BaseTool):
    def __init__(self):
        self.name = "一键安装CMAKE"
        self.type = BaseTool.TYPE_INSTALL
        self.autor = 'xxrz'

    def tool_install_4(self):
        PrintUtils.print_info("开始安装CMAKE")
        CmdTask('tar -zxvf ~/autoware/cmake-3.26.0-rc3.tar.gz -C ~/autoware', os_command=True).run()
        CmdTask('cd ~/autoware/cmake-3.26.0-rc3 && ./bootstrap && make && sudo make install', os_command=True).run()
        CmdTask('cmake --version', os_command=True).run()
        PrintUtils.print_success("终端输出3.26.0-rc3即TCMAKE安装完成")
        PrintUtils.print_info("\033[41m"+"\033[3m"+"\033[1m"+"当前选择的是-----[5]，下一个请选择----[6]"+"\033[0m", 0.001)

    def run(self):
        self.tool_install_4()
