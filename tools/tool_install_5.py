# -*- coding: utf-8 -*-
from .base import BaseTool
from .base import PrintUtils, CmdTask, FileUtils, AptUtils, ChooseTask
from .base import osversion, osarch
from .base import run_tool_file

#  PrintUtils-打印信息
#  CmdTask-运行命令
#  FileUtils-文件操作
#  AptUtils-软件包管理
#  ChooseTask-显示选项列表


class Tool(BaseTool):
    def __init__(self):
        self.name = "一键安装eigen-3.3.7"
        self.type = BaseTool.TYPE_INSTALL
        self.autor = 'xxrz'

    def tool_install_4(self):
        PrintUtils.print_info("开始安装eigen-3.3.7")
        CmdTask('tar -zxvf ~/autoware/eigen-3.3.7.tar.gz -C ~/autoware',os_command=True).run()
        CmdTask('cd ~/autoware/eigen-3.3.7 && mkdir build && cd build && cmake .. && sudo make install',os_command=True).run()
        CmdTask('sudo cp -r /usr/local/include/eigen3/Eigen /usr/local/include ',os_command=True).run()
        CmdTask('pkg-config --modversion eigen3',os_command=True).run()
        PrintUtils.print_success("终端输出3.3.7即TCMAKE安装完成")
        PrintUtils.print_info("\033[41m"+"\033[3m"+"\033[1m"+"当前选择的是-----[6]，下一个请选择----[7]"+"\033[0m", 0.001)

    def run(self):
        self.tool_install_4()
