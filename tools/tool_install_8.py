# -*- coding: utf-8 -*-
from .base import BaseTool
from .base import PrintUtils, CmdTask, FileUtils, AptUtils, ChooseTask
from .base import osversion, osarch
from .base import run_tool_file

#  PrintUtils-打印信息
#  CmdTask-运行命令
#  FileUtils-文件操作
#  AptUtils-软件包管理
#  ChooseTask-显示选项列表


class Tool(BaseTool):
    def __init__(self):
        self.name = "一键配置autoware依赖"
        self.type = BaseTool.TYPE_INSTALL
        self.autor = 'xxrz'

    def tool_install_8(self):
        PrintUtils.print_info("开始配置autoware依赖")
        CmdTask('cd ~/autoware/Other/autoware.release && sudo apt update && sudo apt install -y python-catkin-pkg python-rosdep ros-melodic-catkin && sudo apt install -y python3-pip python3-colcon-common-extensions python3-setuptools python3-vcstool && pip3 install -U setuptools -i https://pypi.tuna.tsinghua.edu.cn/simple && rosdepc update && rosdep install -y --from-paths src --ignore-src --rosdistro $ROS_DISTRO', os_command=True).run()
        PrintUtils.print_success("autoware依赖配置完成(部分依赖可能无法安装，请运行【10】autoware依赖补丁--进行缺失依赖安装)")
        PrintUtils.print_info("\033[41m"+"\033[3m"+"\033[1m"+"当前选择的是-----[9]，下一个请选择----[10]"+"\033[0m", 0.001)

    def run(self):
        self.tool_install_8()
