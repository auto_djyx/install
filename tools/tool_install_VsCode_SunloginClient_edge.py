# -*- coding: utf-8 -*-
from .base import BaseTool
from .base import PrintUtils,CmdTask,FileUtils,AptUtils,ChooseTask
from .base import osversion,osarch
from .base import run_tool_file

class Tool(BaseTool):
    def __init__(self):
        self.name = "一键安装Vscode"
        self.type = BaseTool.TYPE_INSTALL
        self.autor = 'xxrz'

    def install_VSE(self):
        PrintUtils.print_info("开始安装三剑~")
        PrintUtils.print_info("第一剑~")
        CmdTask("sudo dpkg -i ~/autoware/Software/code_1.72.2-1665614327_amd64.deb").run()
        PrintUtils.print_info("第二剑~~")
        CmdTask("sudo dpkg -i ~/autoware/Software/SunloginClient_11.0.1.44968_amd64.deb").run()
        PrintUtils.print_info("第三剑~~~")
        CmdTask("sudo dpkg -i ~/autoware/Software/microsoft-edge-stable_114.0.1823.67-1_amd64.deb").run()
        PrintUtils.print_info("恭喜你完成三剑安装~")

    def run(self):
        self.install_VSE()

