# -*- coding: utf-8 -*-
from .base import BaseTool
from .base import PrintUtils,CmdTask,FileUtils,AptUtils,ChooseTask
from .base import osversion,osarch
from .base import run_tool_file

class Tool(BaseTool):
    def __init__(self):
        self.name = "一键安装google(浏览器)"
        self.type = BaseTool.TYPE_INSTALL
        self.autor = 'xxrz'

    def install_google(self):
        PrintUtils.print_info("开始安装google浏览器~")
        CmdTask("sudo dpkg -i ~/autoware/Software/google-chrome-stable_current_amd64.deb").run()
        PrintUtils.print_info("安装完成~")

    def run(self):
        self.install_google()

