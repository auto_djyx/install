# -*- coding: utf-8 -*-
from .base import BaseTool
from .base import PrintUtils, CmdTask, FileUtils, AptUtils, ChooseTask
from .base import osversion, osarch
from .base import run_tool_file

class Tool(BaseTool):
    def __init__(self):
        self.name = "一键安装opencv 3.4.5"
        self.type = BaseTool.TYPE_INSTALL
        self.autor = 'xxrz'

    def tool_install_6(self):
        PrintUtils.print_info("开始安装opencv 3.4.5")
        CmdTask('sudo apt-get update',os_command=True).run()
        CmdTask('sudo apt-get install -y build-essential cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev',os_command=True).run()
        CmdTask('cd ~/autoware/opencv-3.4.5 && cd opencv && git checkout 3.4.5 && mkdir build && cd build && cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local .. && make -j4 && sudo make install',os_command=True).run()
        CmdTask('pkg-config --modversion opencv',os_command=True).run()
        PrintUtils.print_success("终端输出3.4.5即TCMAKE安装完成")
        PrintUtils.print_info("\033[41m"+"\033[3m"+"\033[1m"+"当前选择的是-----[7]，下一个请选择----[8]"+"\033[0m", 0.001)

    def run(self):
        self.tool_install_6()
