# -*- coding: utf-8 -*-
from .base import BaseTool
from .base import PrintUtils, CmdTask, FileUtils, AptUtils, ChooseTask
from .base import osversion, osarch
from .base import run_tool_file

#  PrintUtils-打印信息
#  CmdTask-运行命令
#  FileUtils-文件操作
#  AptUtils-软件包管理
#  ChooseTask-显示选项列表


class Tool(BaseTool):
    def __init__(self):
        self.name = "autoware依赖补丁"
        self.type = BaseTool.TYPE_INSTALL
        self.autor = 'xxrz'

    def tool_install_9(self):
        PrintUtils.print_info("开始配置autoware依赖")
        CmdTask('sudo apt-get install -y libglew-dev ; sudo apt-get install -y ros-melodic-serial ; sudo apt-get install -y ros-melodic-geodesy ; sudo apt-get install -y ros-melodic-nmea-msgs ; sudo apt-get install -y ros-melodic-gps-common ; sudo apt-get install -y ros-melodic-sound-play ; sudo apt-get install -y ros-melodic-grid-map-cv ; sudo apt-get install -y ros-melodic-lanelet2-io ; sudo apt-get install -y ros-melodic-grid-map-ros ; sudo apt-get install -y ros-melodic-grid-map-ros ; sudo apt-get install -y ros-melodic-lanelet2-core ; sudo apt-get install -y ros-melodic-grid-map-msgs ; sudo apt-get install -y ros-melodic-lanelet2-maps ; sudo apt-get install -y ros-melodic-qpoases-vendor ; sudo apt-get install -y ros-melodic-jsk-rviz-plugin ; sudo apt-get install -y ros-melodic-lanelet2-routing ; sudo apt-get install -y ros-melodic-nmea-navsat-driver ; sudo apt-get install -y ros-melodic-velodyne-pointcloud ; sudo apt-get install -y ros-melodic-lanelet2-validation ; sudo apt-get install -y ros-melodic-lanelet2-projection ; sudo apt-get install -y ros-melodic-jsk-recognition-msgs ; sudo apt-get install -y ros-melodic-automotive-platform-msgs ; sudo apt-get install -y ros-melodic-automotive-navigation-msgs', os_command=True).run()
        PrintUtils.print_success("autoware依赖配置完成")

    def run(self):
        self.tool_install_9()
