# -*- coding: utf-8 -*-
from .base import BaseTool
from .base import PrintUtils, CmdTask, FileUtils, AptUtils, ChooseTask
from .base import osversion, osarch
from .base import run_tool_file

#  PrintUtils-打印信息
#  CmdTask-运行命令
#  FileUtils-文件操作
#  AptUtils-软件包管理
#  ChooseTask-显示选项列表


class Tool(BaseTool):
    def __init__(self):
        self.name = "一键安装CloudCompare"
        self.type = BaseTool.TYPE_INSTALL
        self.autor = 'xxrz'

    def CloudCompare(self):
        PrintUtils.print_info("开始安装CloudCompare")
        CmdTask('sudo snap install cloudcompare', os_command=True).run()
        PrintUtils.print_success("CloudCompare安装完成")

    def run(self):
            self.CloudCompare()
