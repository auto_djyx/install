# -*- coding: utf-8 -*-
from .base import BaseTool
from .base import PrintUtils, CmdTask, FileUtils, AptUtils, ChooseTask
from .base import osversion, osarch
from .base import run_tool_file

#  PrintUtils-打印信息
#  CmdTask-运行命令
#  FileUtils-文件操作
#  AptUtils-软件包管理
#  ChooseTask-显示选项列表


class Tool(BaseTool):
    def __init__(self):
        self.name = "一键设置USB权限"
        self.type = BaseTool.TYPE_INSTALL
        self.autor = 'xxrz'

    def tool_install_7(self):
        PrintUtils.print_info("开始设置USB权限")
        CmdTask('sudo cp ~/autoware/USBCAN/r.md /etc/udev/rules.d/99-myusb.rules', os_command=True).run()
        CmdTask('sudo cp -r ~/autoware/USBCAN/x86/libcontrolcan.a /usr/local/lib', os_command=True).run()
        CmdTask('sudo cp -r ~/autoware/USBCAN/x86/libcontrolcan.so /usr/local/lib', os_command=True).run()
        PrintUtils.print_success("设置USB权限完成")
        PrintUtils.print_info("\033[41m"+"\033[3m"+"\033[1m"+"当前选择的是-----[8]，下一个请选择----[9]"+"\033[0m", 0.001)

    def run(self):
        self.tool_install_7()
