# -*- coding: utf-8 -*-
from .base import BaseTool
from .base import PrintUtils, CmdTask, FileUtils, AptUtils, ChooseTask
from .base import osversion, osarch
from .base import run_tool_file

class Tool(BaseTool):
    def __init__(self):
        self.name = "安装CUDA"
        self.type = BaseTool.TYPE_INSTALL
        self.autor = 'xxrz'

    def tool_install_3(self):
        PrintUtils.print_info("开始安装CUDA")
        CmdTask("gnome-terminal --window -- bash -c 'xdg-open ~/autoware/READE.md'",os_command=True).run()
        CmdTask('gnome-terminal --window -- bash -c "cd ~/autoware;sudo sh cuda_10.2.89_440.33.01_linux.run" ', os_command=True).run()
        input("手动安装CUDA完成后，按回车继续...")
        CmdTask("sed -i '$aexport PATH=/usr/local/cuda-10.2/bin${PATH:+:${PATH}}' ~/.bashrc", os_command=True).run()
        CmdTask("sed -i '$aexport LD_LIBRARY_PATH=/usr/local/cuda-10.2/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}' ~/.bashrc", os_command=True).run()
        PrintUtils.print_success("任意终端使用\nnvcc -V \n验证CUDA安装是否成功，输出V10.2.89，安装成功")
        PrintUtils.print_info("\033[41m"+"\033[3m"+"\033[1m"+"当前选择的是-----[4]，下一个请选择----[5]"+"\033[0m", 0.001)
    def run(self):
        self.tool_install_3()
