# -*- coding: utf-8 -*-
from .base import BaseTool
from .base import PrintUtils, CmdTask, FileUtils, AptUtils, ChooseTask
from .base import osversion, osarch
from .base import run_tool_file
import time
import os

class Tool(BaseTool):
    def __init__(self):
        self.name = "安装Nvidia显卡驱动"
        self.type = BaseTool.TYPE_INSTALL
        self.autor = 'xxrz'

    def tool_install_2(self):
        PrintUtils.print_info("开始安装Nvidia显卡驱动")
        # 安装Nvidia显卡驱动
        CmdTask('sudo apt-get update', os_command=True).run()
        CmdTask('sudo apt-get install nvidia-driver-470 -y', os_command=True).run()
        PrintUtils.print_info("安装Nvidia显卡驱动完成")
        # 延迟10秒后重启
        PrintUtils.print_success(
        '已为您安装好nvidia-driver-470,重启后在任意终端使用:\nnvidia-smi \n验证显卡驱动,电脑将10秒后将自动重启.......')
        PrintUtils.print_info("\033[41m"+"\033[3m"+"\033[1m"+"当前选择的是-----[3]，下一个请选择----[4]"+"\033[0m", 0.001)
        time.sleep(10)
        os.system('sudo reboot')

    def run(self):
        self.tool_install_2()
