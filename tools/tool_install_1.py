# -*- coding: utf-8 -*-
from .base import BaseTool
from .base import PrintUtils, CmdTask, FileUtils, AptUtils, ChooseTask
from .base import osversion, osarch
from .base import run_tool_file
import time
import os


class Tool(BaseTool):
    def __init__(self):
        self.name = "禁用显卡图形模式和nouveau"
        self.type = BaseTool.TYPE_INSTALL
        self.autor = 'xxrz'

    def tool_install_1(self):
        PrintUtils.print_info("开始禁用显卡图形模式")
        CmdTask('sed -i \'s/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash nomodeset"/g\' /etc/default/grub', os_command=True).run()
        CmdTask('sudo update-grub', os_command=True).run()
        PrintUtils.print_info("禁用显卡图形模式完成")

        # 禁用nouveau驱动
        PrintUtils.print_info("开始禁用nouveau驱动")
        CmdTask('sudo sh -c "echo \'blacklist nouveau\' >> /etc/modprobe.d/blacklist-nouveau.conf"', os_command=True).run()
        CmdTask('sudo sh -c "echo \'options nouveau modeset=0\' >> /etc/modprobe.d/blacklist-nouveau.conf"', os_command=True).run()
        CmdTask('sudo update-initramfs -u', os_command=True).run()
        PrintUtils.print_info("禁用nouveau驱动完成")
        PrintUtils.print_info("开始Terminator终端安装")
        CmdTask('sudo apt install terminator -y', os_command=True).run()
        PrintUtils.print_info("Terminator终端安装完成")
        CmdTask("sudo apt install python3-pip -y", 0).run()
        CmdTask(
            "sudo pip3 install -i https://pypi.tuna.tsinghua.edu.cn/simple rosdepc", 0).run()
        CmdTask("sudo rosdepc init", 0).run()
        CmdTask("sudo rosdepc fix-permissions", 0).run()
        PrintUtils.print_info(
            '已为您安装好rosdepc,请使用:\nrosdepc update \n进行测试更新,')
        # 延迟10秒后重启
        PrintUtils.print_success("将10秒后将自动重启...")
        PrintUtils.print_info("\033[41m"+"\033[3m"+"\033[1m"+"当前选择的是-----[1]，下一个请选择----[2]"+"\033[0m", 0.001)
        time.sleep(10)
        os.system('sudo reboot')

    def run(self):
        self.tool_install_1()
